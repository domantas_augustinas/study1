var config = {

    deps: [
        "js/main",
    ],

    map: {
        '*': {
            'bootstrap': 'js/bootstrap.min'

        }
    },
    "shim": {
        "bootstrap": ["jquery"]
    }
};
