<?php

namespace Learning\CategoryImageResizer\ViewModel;

use Magento\Framework\Filesystem;
use Magento\Framework\Image\AdapterFactory;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;

class CatalogViewModel implements ArgumentInterface
{

    protected $filesystem;

    protected $imageFactory;

    protected $storeManager;

    /**
     * @param Filesystem $filesystem
     * @param AdapterFactory $imageFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Filesystem $filesystem,
        AdapterFactory $imageFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->filesystem = $filesystem;
        $this->imageFactory = $imageFactory;
        $this->storeManager = $storeManager;
    }

    /**
     * @param string $imageUrl
     *
     * @return string
     * @throws \Exception
     */
    public function resizeImage($imageUrl): string
    {

        $imageWidth = 50;
        $imageHeight = 50;

        $mediaPath = UrlInterface::URL_TYPE_MEDIA;
        $imageName = trim(basename($imageUrl) . PHP_EOL);

        $absolutePath = $this->filesystem->getDirectoryRead($mediaPath)->
            getAbsolutePath('catalog/category/') . $imageName;
        $imageResizedPath = $this->filesystem->getDirectoryRead($mediaPath)->
            getAbsolutePath('catalog/category/resized/' . $imageWidth . '/') . $imageName;

        $resizedURL = $this->storeManager->getStore()->
            getBaseUrl(UrlInterface::URL_TYPE_MEDIA)
                      . 'catalog/category/resized/' . $imageWidth . '/' . $imageName;

        if (file_exists($imageResizedPath)) {
            return $resizedURL;
        }
        //create image factory...
        $imageResize = $this->imageFactory->create();
        $imageResize->open($absolutePath);
        $imageResize->constrainOnly(true);
        $imageResize->keepTransparency(true);
        $imageResize->keepFrame(false);
        $imageResize->keepAspectRatio(true);
        $imageResize->resize($imageWidth, $imageHeight);
        //save image to destination path
        $imageResize->save($imageResizedPath);

        return $resizedURL;
    }

}
