<?php

namespace Learning\ModuleNotice\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

	const XML_PATH_MODULE_NOTICE = 'modulenotice/general/';

    /**
     * @param string $field
     * @return string|null
     */
	public function getConfigValue(string $field): ?string
    {
		return $this->scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE
		);
	}

    /**
     * @param string $code
     * @return string|null
     */
	public function getGeneralConfig(string $code): ?string
    {
		return $this->getConfigValue(self::XML_PATH_MODULE_NOTICE . $code);
	}

}

