<?php

namespace Learning\ModuleNotice\Block;

use Learning\ModuleNotice\Helper\Data;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\View\Element\Template;

class NoticeBlock extends Template
{
    protected $NoticeHelperData;

    protected $context;

    public function __construct(
        Context $context,
        Data $NoticeHelperData,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->context = $context;
        $this->NoticeHelperData = $NoticeHelperData;
    }

    /**
     * @return string|null
     */
    public function getDisplayText(): ?string
    {
        return $this->NoticeHelperData->getGeneralConfig('display_text');
    }
}
