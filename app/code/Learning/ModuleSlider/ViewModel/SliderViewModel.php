<?php

namespace Learning\ModuleSlider\ViewModel;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class SliderViewModel implements ArgumentInterface
{

    const SLIDER_DISPLAY_STATUS = 'moduleslider/general/active';

    protected $scopeConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return bool
     */
    public function displayStatus(): bool
    {
        $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
        return $this->scopeConfig->isSetFlag(self::SLIDER_DISPLAY_STATUS, $scopeType);
    }
}
