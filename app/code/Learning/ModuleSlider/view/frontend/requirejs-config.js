var config = {

    paths: {
        owlcarousel : 'Learning_ModuleSlider/js/owl.carousel'
    },
    shim: {
        owlcarousel: {
            deps: ['jquery']
        }
    }
};
