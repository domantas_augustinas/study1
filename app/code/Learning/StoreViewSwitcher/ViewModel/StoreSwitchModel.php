<?php

namespace Learning\StoreViewSwitcher\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\ResourceModel\Website\Collection as WebsiteCollection;
use Magento\Store\Model\ResourceModel\Website\CollectionFactory as WebsiteCollectionFactory;

class StoreSwitchModel implements ArgumentInterface
{

    private $websiteCollectionFactory;

    public function __construct(
        WebsiteCollectionFactory $websiteCollectionFactory
    ) {
        $this->websiteCollectionFactory = $websiteCollectionFactory;
    }

    /**
     * @return WebsiteCollection
     */
    public function getWebsites(): WebsiteCollection
    {
        return $this->websiteCollectionFactory->create();
    }

    /**
     * @param StoreInterface $store
     * @return string
     */
    public function getStoreSwitchLabel(StoreInterface $store): string
    {
        return $store->getName();
    }
}
