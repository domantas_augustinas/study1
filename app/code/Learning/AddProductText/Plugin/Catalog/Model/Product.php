<?php

namespace Learning\AddProductText\Plugin\Catalog\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Catalog\Model\Product as CatalogProduct;

class Product
{
    const XML_PATH_TO_CONFIG = 'moduleproductaddtext/general/add_product_text';

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param CatalogProduct $product
     * @param string $productTitle
     * @return string
     */
    public function afterGetName(CatalogProduct $product, string $productTitle): string
    {
        $storeScope = ScopeInterface::SCOPE_STORE;
	    $addTextValue = $this->scopeConfig->getValue(self::XML_PATH_TO_CONFIG, $storeScope);

        return $productTitle . " " . $addTextValue;
    }
}
